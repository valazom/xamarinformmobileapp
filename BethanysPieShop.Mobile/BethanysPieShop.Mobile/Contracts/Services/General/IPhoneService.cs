﻿using BethanysPieShop.Mobile.Core.CustomAttributes;

namespace BethanysPieShop.Mobile.Core.Contracts.Services.General
{
    
    public  interface IPhoneService
    {
        void MakePhoneCall();
    }
}
