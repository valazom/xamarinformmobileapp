﻿using System.Threading.Tasks;

namespace BethanysPieShop.Mobile.Core.Contracts.Repository
{
    public interface IGenericRepository
    {
        string Url { get; set; }
        string AuthToken { get; set; }
        Task<T> GetAsync<T>();
        Task<T> PostAsync<T>(T data);
        Task<T> PutAsync<T>(T data);
        Task DeleteAsync();
        Task<R> PostAsync<T, R>(T data);
    }
}
