﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BethanysPieShop.Mobile.Core.CustomAttributes
{
    public class ViewModelAttribute : Attribute
    {
        public bool IsSingleton { get; set; }
        public ViewModelAttribute(bool isSingleton = false)
        {
            IsSingleton = isSingleton;
        }
    }
}
