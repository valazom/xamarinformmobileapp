﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BethanysPieShop.Mobile.Core.CustomAttributes
{
    public class ServiceContract: Attribute
    {
        public bool IsSingleton { get; set; }
        public ServiceContract(bool isSingleton = false)
        {
            IsSingleton = isSingleton;
        }
    }
}
