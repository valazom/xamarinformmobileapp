﻿using BethanysPieShop.Mobile.Core.Contracts.Services.General;
using BethanysPieShop.Mobile.Core.CustomAttributes;
using Plugin.Messaging;

namespace BethanysPieShop.Mobile.Core.Services.General
{
    [ServiceContract]
    public class PhoneService: IPhoneService
    {
        public void MakePhoneCall()
        {
            CrossMessaging.Current.PhoneDialer.MakePhoneCall("5554885002");
        }
    }
}
