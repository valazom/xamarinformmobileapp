﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Akavache;
using BethanysPieShop.Mobile.Core.Constants;
using BethanysPieShop.Mobile.Core.Contracts.Services.Data;
using BethanysPieShop.Mobile.Core.Extensions;
using BethanysPieShop.Mobile.Core.Models;
using System.Reactive.Linq;
using BethanysPieShop.Mobile.Core.Contracts.Repository;
using BethanysPieShop.Mobile.Core.CustomAttributes;

namespace BethanysPieShop.Mobile.Core.Services.Data
{
    [ServiceContract]
    public class CatalogDataService : BaseService, ICatalogDataService
    {
        private readonly IGenericRepository _genericRepository;

        public CatalogDataService(IGenericRepository genericRepository, 
            IBlobCache cache = null) : base(cache)
        {
            _genericRepository = genericRepository;
        }

        public async Task<IEnumerable<Pie>> GetAllPiesAsync()
        {
            SetApiUrl();
            List<Pie> pies =
                await GetOrFetchCache(CacheNameConstants.AllPies, _genericRepository.GetAsync<List<Pie>>);

            return pies;
        }
        public async Task<IEnumerable<Pie>> GetPiesOfTheWeekAsync()
        {
            SetApiUrl();
            List<Pie> pies =
                await GetOrFetchCache(CacheNameConstants.PiesOfTheWeek, _genericRepository.GetAsync<List<Pie>>,
                DateTimeOffset.Now.AddSeconds(20));

            return pies;
        }
        private void SetApiUrl()
        {
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.CatalogEndpoint
            };
            _genericRepository.Url = builder.ToString();
        }
    }
}
