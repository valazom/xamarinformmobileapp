﻿using Akavache;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reactive.Linq;
using BethanysPieShop.Mobile.Core.Models;
using System;

namespace BethanysPieShop.Mobile.Core.Services.Data
{
    public class BaseService
    {
        protected IBlobCache Cache;

        public BaseService(IBlobCache cache)
        {
            Cache = cache ?? BlobCache.LocalMachine;
        }

        public async Task<T> GetFromCache<T>(string cacheName)
        {
            try
            {
                T t = await Cache.GetObject<T>(cacheName);
                return t;
            }
            catch (KeyNotFoundException)
            {
                return default(T);
            }
        }

        public async Task<T> GetOrFetchCache<T>(string cacheName,Func<Task<T>> fetchFunc, DateTimeOffset? expirationDate = null)
        {
            return await Cache.GetOrFetchObject<T>(cacheName, fetchFunc, expirationDate);
        }

        public void InvalidateCache<T>()
        {
            Cache.InvalidateAllObjects<T>();
        }
    }
}
