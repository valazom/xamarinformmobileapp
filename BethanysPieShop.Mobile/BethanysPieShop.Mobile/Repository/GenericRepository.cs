﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BethanysPieShop.Mobile.Core.Contracts.Repository;
using BethanysPieShop.Mobile.Core.CustomAttributes;
using BethanysPieShop.Mobile.Core.Exceptions;
using Newtonsoft.Json;
using Polly;

namespace BethanysPieShop.Mobile.Core.Repository
{
    [ServiceContract]
    public class GenericRepository : IGenericRepository
    {
        public string Url { get; set ; }

        public string AuthToken { get; set; }

        public async Task<T> GetAsync<T>()
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(Url);
                string jsonResult = string.Empty;

                var responseMessage = await ServiceCall(httpClient);

                var dataResult = await ExtractJsonData<T>(jsonResult, responseMessage);

                if (dataResult != null) return dataResult;

                CheckResponseStatus(jsonResult, responseMessage);

                throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonResult);

            }
            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }

        public async Task<T> PostAsync<T>(T data)
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(Url);

                var content = new StringContent(JsonConvert.SerializeObject(data));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string jsonResult = string.Empty;
                HttpResponseMessage responseMessage = await ServiceCall(httpClient, content);

                var dataResult = await ExtractJsonData<T>(jsonResult, responseMessage);

                if (dataResult != null) return dataResult;

                CheckResponseStatus(jsonResult, responseMessage);

                throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonResult);

            }
            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }

        private static async Task<T> ExtractJsonData<T>(string jsonResult, HttpResponseMessage responseMessage)
        {
            if (responseMessage.IsSuccessStatusCode)
            {
                jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var json = JsonConvert.DeserializeObject<T>(jsonResult);
                return json;
            }

            return default(T);
        }

        private static void CheckResponseStatus(string jsonResult, HttpResponseMessage responseMessage)
        {
            if (responseMessage.StatusCode == HttpStatusCode.Forbidden ||
                                responseMessage.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new ServiceAuthenticationException(jsonResult);
            }
        }

        private async Task<HttpResponseMessage> ServiceCall(HttpClient httpClient, StringContent content = null)
        {
            return await Policy
                    .Handle<WebException>(ex =>
                    {
                        Debug.WriteLine($"{ex.GetType().Name + " : " + ex.Message}");
                        return true;
                    })
                    .WaitAndRetryAsync
                    (
                        5,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))
                    )
                    .ExecuteAsync(async () => await httpClient.PostAsync(Url, content));
        }

        public async Task<TR> PostAsync<T, TR>(T data)
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(Url);

                var content = new StringContent(JsonConvert.SerializeObject(data));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string jsonResult = string.Empty;

                var responseMessage = await ServiceCall(httpClient, content);

                var dataResult = await ExtractJsonData<TR>(jsonResult, responseMessage);

                if (dataResult != null) return dataResult;

                CheckResponseStatus(jsonResult, responseMessage);

                throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonResult);

            }
            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }

        public async Task<T> PutAsync<T>(T data)
        {
            try
            {
                HttpClient httpClient = CreateHttpClient(Url);

                var content = new StringContent(JsonConvert.SerializeObject(data));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string jsonResult = string.Empty;

                var responseMessage = await ServiceCall(httpClient, content);

                var dataResult = await ExtractJsonData<T>(jsonResult, responseMessage);

                if (dataResult != null) return dataResult;

                CheckResponseStatus(jsonResult, responseMessage);

                throw new HttpRequestExceptionEx(responseMessage.StatusCode, jsonResult);

            }
            catch (Exception e)
            {
                Debug.WriteLine($"{ e.GetType().Name + " : " + e.Message}");
                throw;
            }
        }

        public async Task DeleteAsync()
        {
            HttpClient httpClient = CreateHttpClient(AuthToken);
            await httpClient.DeleteAsync(Url);
        }

        private HttpClient CreateHttpClient(string authToken)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!string.IsNullOrEmpty(authToken))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
            }
            return httpClient;
        }

        
    }
}
