﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using BethanysPieShop.Mobile.Core.Contracts.Repository;
using BethanysPieShop.Mobile.Core.Contracts.Services.Data;
using BethanysPieShop.Mobile.Core.Contracts.Services.General;
using BethanysPieShop.Mobile.Core.CustomAttributes;
using BethanysPieShop.Mobile.Core.Repository;
using BethanysPieShop.Mobile.Core.Services.Data;
using BethanysPieShop.Mobile.Core.Services.General;
using BethanysPieShop.Mobile.Core.ViewModels;

namespace BethanysPieShop.Mobile.Core.Bootstrap
{
    public class AppContainer
    {
        private static IContainer _container;

        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            //ViewModels
            RegisterViewModels(builder);

            //services - data
            RegisterDataServiceTypes(builder);

            _container = builder.Build();
        }

        private static void RegisterDataServiceTypes(ContainerBuilder builder)
        {
            
            var interfaceTypes = GetInterfaceTypes();
            var assignableClassTypes = GetAttibuteTypes<ServiceContract>();
            foreach (var interfaceType in interfaceTypes)
            {
                var classType = assignableClassTypes.SingleOrDefault(x => interfaceType.IsAssignableFrom(x));

                if (classType != null)
                {
                    var attr = Attribute.GetCustomAttribute(classType, typeof(ServiceContract));

                    if (attr != null)
                    {
                        if ((attr as ServiceContract).IsSingleton)
                            builder.RegisterType(classType).As(interfaceType).SingleInstance();
                        else
                            builder.RegisterType(classType).As(interfaceType);
                    } 
                }

            }
           
            
        }

        private static void RegisterViewModels(ContainerBuilder builder)
        {
            System.Collections.Generic.IEnumerable<Type> viewModelTypes = GetAttibuteTypes<ViewModelAttribute>();
            foreach (var type in viewModelTypes)
            {
                var attr = (ViewModelAttribute)Attribute.GetCustomAttribute(type, typeof(ViewModelAttribute));
                if (attr.IsSingleton)
                    builder.RegisterType(type).SingleInstance();

                else
                    builder.RegisterType(type);
            }
        }

        private static IEnumerable<Type> GetAttibuteTypes<T>() where T : Attribute
        {
            var assembly = Assembly.GetExecutingAssembly();
            var viewModelTypes = assembly.GetTypes().Where(x => x.GetCustomAttributes(typeof(T), true).Length > 0);
            return viewModelTypes;
        }

        private static IEnumerable<Type> GetInterfaceTypes()
        {
            return Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsInterface);
        }

        public static object Resolve(Type typeName)
        {
            return _container.Resolve(typeName);
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
